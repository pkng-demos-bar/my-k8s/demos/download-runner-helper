# download-runner-helper

GitLab runner leverages on a runner helper container image[[1]], to handle Git, artifacts and cache operations. For an offline setup, this image needs to be pulled from [[2]] and stored in your local GitLab container registry. 

This project is referenced from [[3]] and seeks to demostrate similar capability for the runner helper.

Additionally, it also outputs the tarball of the image for download in the package registry.





## References
Helper Image [[1]]  
GitLab runner helper container registry [[2]]  
Loading docker images onto your offline host [[3]]   

[1]:https://docs.gitlab.com/runner/configuration/advanced-configuration.html#helper-image "Helper Image"
[2]:https://gitlab.com/gitlab-org/gitlab-runner/container_registry/1472754 "GitLab runner helper container registry"
[3]:https://docs.gitlab.com/ee/user/application_security/offline_deployments/#loading-docker-images-onto-your-offline-host "Loading docker images onto your offline host"